# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY /redemption-promo-catalog-1.2.0-SNAPSHOT.jar /redemption-promo-catalog.jar
# run application with this command line[
CMD ["java", "-jar", "/redemption-promo-catalog.jar"]
RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
